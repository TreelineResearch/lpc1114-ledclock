/*
    \brief Initialize timer0 
    
    Generates a 400khz interrupt to use as a bit clock for the WS2811
    when TIMER_16_0_IRQn is enabled
*/
void timer0_init() {
    LPC_SYSCON->SYSAHBCLKCTRL |= (1<<7);
    
    LPC_TMR16B0->PR = 0x0; // no prescale 
    LPC_TMR16B0->MCR = 0x3; // timer 0 interrupt on match and reset
    LPC_TMR16B0->EMR = 0x0;
    LPC_TMR16B0->CCR = 0x0;
    LPC_TMR16B0->PWMC = 0x0;
    LPC_TMR16B0->MR0 = 92; // assuming a crystal of 2*18.432MHZ, divide by 92 to get 400kHz
    LPC_TMR16B0->TCR |= 0x3;
    LPC_TMR16B0->TCR &= ~(0x2);
    LPC_TMR16B0->IR = 0xFF; // clear the interrupts
}

	        //seg_pfill(pixel_buf, CHARGEN[36], 0, 0x00020202);
	        //seg_modulate(pixel_buf, 36, 0, &mod_rainbow_lit, Tick % 360 + 270);
	        seg_puts(pixel_buf, (char *)msg[0], 0x000E0F0F, 0x00000000);
	        buf_modulate(pixel_buf, 36, 0, modfn_table[0], (uint32_t)(((G_Tick % (TICKRATE*60))/(TICKRATE*60.0F))*359.0F));
	        //seg_modulate(pixel_buf, 36, 0, &mod_gradsweep, (uint32_t)((float)(q/10)*36.0F));
	        //buf_modulate(pixel_buf, 36, 0, &mod_flicker, NULL);
	        ws2811_reset(CHAN_A_GPIO, CHAN_A_pin);
	        ws2811_send(CHAN_A_GPIO, CHAN_A_pin, pixel_buf, NPIXELS);
	        /*
	        // a doobie!
	        seg_pfill(pixel_buf, 0x0180, 0, 0x00020202);
	        seg_pfill(pixel_buf, 0x0240, 0, 0x00102000);
	        seg_pfill(pixel_buf, 0x7C3E, 0, 0x00202020);
	        seg_pfill(pixel_buf, 0x8001, 0, 0x00200000);
	        */	        
//seg_puts(pixel_buf, (char *)msg[cmsg], hsl_to_rbg((Tick / 30.0)*359.0, 1.0, 0.2), 0x00030000);	        

	        //buf_modulate(pixel_buf, 16, 0, modfn_table[4], (G_Tick*16) % 360);
	        // MODF B [0 $ENV_TICK 16 * 360 %]
	            /*
    modfn_table[0] = &mod_rainbow_lit;
    modfn_table[1] = &mod_flicker_lit;
    modfn_table[2] = &mod_window;
    modfn_table[3] = &mod_windowi;
    modfn_table[4] = &mod_hacker_lit;
    */


uint32_t pixel_adds(uint32_t pixel, int16_t r, int16_t b, int16_t g) {
    uint32_t out = 0;
    uint32_t x = 0;
    x = ((pixel >> 16) & 0xFF) + r;
    x = (x > 0xFF) ? 0xFF: x;
    x = (x < 0) ? 0: x;
    out = x << 16;
    x = ((pixel >> 8) & 0xFF) + b;
    x = (x > 0xFF) ? 0xFF: x;
    x = (x < 0) ? 0: x;
    out |= x << 8;
    x = (pixel & 0xFF) + g;
    x = (x > 0xFF) ? 0xFF: x;
    x = (x < 0) ? 0: x;
    out |= x;
    return out;
}