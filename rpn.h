#include <fix16.h>
#ifndef __RPN_H__
#define __RPN_H__
#define RPN_WBUFLEN 8
#define RPN_MAX_ITER 10
#define RPN_STACK_MAX 8
#define RPN_STACK_EMPTY -1
#define RPN_STACK_OVERFLOW -99

#define RPN_READ_FLAG 1
#define RPN_POINT_FLAG 2

typedef union {
    fix16_t f;
    struct {
        int16_t frac;
        uint16_t i;
    };
} fixint_t;

int8_t rpn_push(fixint_t val);
fixint_t rpn_pop(void);

fixint_t rpn_evaluate(char *program, channel_t *c, uint32_t *env, fix16_t *scratch, fix16_t *out);

#endif
