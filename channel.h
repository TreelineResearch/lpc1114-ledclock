#ifndef __CHANNEL_H__
#define __CHANNEL_H__

#include <stdint.h>
#include <fix16.h>
#include "config.h"
#include "common.h"

#define CHAN_FLAG_SEG      1
#define CHAN_FLAG_PAT      2
#define CHAN_FLAG_SOLID_FG 4
#define CHAN_FLAG_SOLID_BG 8

#define SEG_A 1
#define SEG_B 2
#define SEG_C 4
#define SEG_D 8
#define SEG_E 16
#define SEG_F 32
#define SEG_G 64
#define SEG_H 128
#define SEG_I 256

void seg_putc(pixel_t buf[], uint8_t c, uint8_t offset, uint32_t fg, uint32_t bg);
void seg_puts(pixel_t buf[], uint8_t *s, uint32_t fg, uint32_t bg);
void buf_patfill(pixel_t outbuf[],
                 uint8_t outbuflen,
                 uint8_t pbuf[],
                 uint8_t patlen,
                 uint8_t offset,
                 uint32_t fgcolor,
                 uint32_t bgcolor);
pixel_t pixel_from_fix16(fix16_t r, fix16_t g, fix16_t b);
void pixel_to_fix16(pixel_t pixel, fix16_t *fixrgb);
pixel_t pixel_muls(pixel_t pixel, fix16_t m);
pixel_t hsl_to_rgb(fix16_t hf, fix16_t sf, fix16_t lf);
void buf_modulate(pixel_t buf[], uint8_t len, uint8_t offset, modfn_ptr_t, fix16_t *arg_list);

void channel_init(channel_t *c, LPC_GPIO_TypeDef *gpio, uint32_t pin, uint8_t length);
void channel_render(channel_t *c, uint32_t *env, fix16_t *scratch);

/*
    \brief Character generator table
    
    The segment layout for a character looks like this:
                            
                         D   
                     E   I-> C
                         H 
                     F   G   B
                   din ->A 
    
*/
static const uint16_t CHARGEN[39] = {
    // 9 segment numbers
    SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F, // zero
    SEG_B | SEG_C, // one
    SEG_A | SEG_C | SEG_D | SEG_F | SEG_H, // two
    SEG_A | SEG_B | SEG_C | SEG_D | SEG_H, // three
    SEG_B | SEG_C | SEG_E | SEG_H, // four
    SEG_A | SEG_B | SEG_D | SEG_E | SEG_H, // five
    SEG_A | SEG_B | SEG_D | SEG_E | SEG_F | SEG_H, // six
    SEG_B | SEG_C | SEG_D, // seven
    SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_H, // eight
    SEG_B | SEG_C | SEG_D | SEG_E | SEG_H, // nine
    // 9 segment letters
    SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_H, // A
    SEG_A | SEG_B | SEG_E | SEG_F | SEG_H, // B
    SEG_A | SEG_F | SEG_H, // C
    SEG_A | SEG_B | SEG_C | SEG_F | SEG_H, // D
    SEG_A | SEG_D | SEG_E | SEG_F | SEG_H, // E
    SEG_D | SEG_E | SEG_F | SEG_H, // F
    SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_H, // G
    SEG_B | SEG_C | SEG_E | SEG_F | SEG_H, // H
    SEG_A | SEG_D | SEG_G | SEG_I, // I
    SEG_A | SEG_B | SEG_C, // J
    SEG_B | SEG_E | SEG_F | SEG_H | SEG_I, // K
    SEG_A | SEG_E | SEG_F, // L
    SEG_B | SEG_F | SEG_G | SEG_H, // M
    SEG_B | SEG_F | SEG_H, // N
    SEG_A | SEG_B | SEG_F | SEG_H, // O
    SEG_C | SEG_D | SEG_E | SEG_F | SEG_H, // P
    SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_G | SEG_F, // Q
    SEG_F | SEG_H, // R
    SEG_A | SEG_B | SEG_D | SEG_E | SEG_H, // S
    SEG_D | SEG_G | SEG_I, // T
    SEG_A | SEG_B | SEG_C | SEG_E | SEG_F, // U
    SEG_C | SEG_E | SEG_F | SEG_G, // V
    SEG_A | SEG_B | SEG_F | SEG_G, // W
    SEG_B | SEG_C | SEG_G | SEG_H | SEG_I, // X (sorta)
    SEG_A | SEG_B | SEG_C | SEG_E | SEG_H, // Y
    SEG_A | SEG_C | SEG_D | SEG_G, // Z (ish)
    // 16 bit patterns
    //BIT(1) | BIT(13) | BIT(14) | BIT(5) | BIT(6) | BIT(9), // clock colon
    0x5555, // odd bits
    0xAAAA // even bits
};
#endif