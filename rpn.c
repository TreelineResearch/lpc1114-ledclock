#include <stdint.h>
#include <stdlib.h>
#include <fix16.h>

#include "config.h"
#include "channel.h"
#include "common.h"
#include "rpn.h"

static char wbuf[RPN_WBUFLEN+1];               // word buffer
static fixint_t rpn_stack[RPN_STACK_MAX];      // the stack
static int8_t rpn_stack_top = RPN_STACK_EMPTY; // the stack top

int8_t rpn_push(fixint_t val) {
    if(rpn_stack_top < RPN_STACK_MAX) {
        rpn_stack[++rpn_stack_top] = val;
        return rpn_stack_top;
    }
    else {
        return RPN_STACK_OVERFLOW;
    }
}

fixint_t rpn_pop() {
    fixint_t ret = {0L};

    if(rpn_stack_top > RPN_STACK_EMPTY) {
        return rpn_stack[rpn_stack_top--];
    }
    else {
        return ret;
    }
}


/**  
    \brief RPN (reverse polish notation)-like calculation interpreter
    
    \note Let's define a language, say something like...
    \verbatim
    N push int literal N
    N E push value of ENV[N]
    N R push value of scratch register N
        0 R  
    V N W pop two, first element is destination scratch register, second is the value to store
        60 0 W set scratch register 0 to 60
    V N P same as above, but with the channel modulator program table
        1 0 P set channel modulator program index 0 to 1
    + pop two, add, push result
    - subtract
    * multiply
    / divide
    % pop modulo pop
    < pop << pop
    > pop >> pop
    \endverbatim
    
    Implement a 360 degree rotation every minute: 
    ((TICKS % (TICKRATE*60)) / (TICKRATE*60)) * 360 step by step

    1E0E60*%0E60* /360*0W; // the space is not relevant, the star slash messes with my editor.
    
    \b Unrolled:
    \verbatim
    bytecode;
            stack
    1E ; // this is two operations, push a number and then execute E
        0 <env[1]> // ticks
    0E ; 
        0 <env[1]>
        1 <env[0]> // tickrate
    60 ;
        0 value of env[1]
        1 value of env[0] 
        2 60 literal
    *  ; (<env[0]> * 60)
        0 value of env[1]
        1 (<env[0]> * 60)
    %  ; 
        0 (<env[1]> % (<env[0]> * 60))
    0E ;
        0 (<env[1]> % (<env[0]> * 60))
        1 <env 0>
    60 ; 
        0 (<env[1]> % (<env[0]> * 60))
        1 <env 0>
        2 60 literal
    *  ; 
        0 (<env[1]> % (<env[0]> * 60))
        1 (<env[0]> * 60)       
    /  ; 
        0 ((<env[1]> % (<env[0]> * 60)) / (<env[0]> * 60))
    360; 
        0 ((<env[1]> % (<env[0]> * 60)) / (<env[0]> * 60))
        1 360 literal
    *  ; // result poops out in stack[0]
        0 ((<env[1]> % (<env[0]> * 60)) / (<env[0]> * 60)) * 360  
    0W ; // write it to scratch 0
    \endverbatim

    That looks ok in principle, and it worked for HP calculators...

    How about a conditional
    \verbatim
    1 0=?1 0W:0 0W!
    (1 = 0)
    ? -> if <pop> != 0 execute from next position
                  == 0 skip until ':' 
    \endverbatim
    
    \note We have no looping constructs, so the "program" can't spin forever.

    Now write something to execute it.
    XXX TODO: thumb JIT codegen ;)
*/

fixint_t rpn_evaluate(char *program, channel_t *c, uint32_t *env, fix16_t *scratch, fix16_t *out) {      
    uint8_t wbufpos = 0; // word buffer position
    uint8_t flags = 0;
    uint8_t pc; // the program counter
    uint8_t inst = '\0'; // the current byte
    uint8_t loopctr = 0;
    uint8_t outp = 0;
    fixint_t temp_a, temp_b;
    for(pc = 0; (inst = program[pc]) != '\0'; pc++) {
        if(ISDIGIT(inst) || inst == '(' || (flags & RPN_READ_FLAG)) {
            switch(inst) {
                case '(':
                    // open paren is a 'quote' so we can pass in negative numbers
                    // without the minus symbol being interpreted as subtraction
                    // (-100)100-100- => (-100 - 100) - 100 => -300

                    // set the read flag if it hasn't been
                    // otherwise ignore the open paren
                    if(!(flags & RPN_READ_FLAG)) {
                        wbufpos = 0;
                        flags |= RPN_READ_FLAG;
                    }
                    break;
                case '-':
                    // if this minus is the first character since we started reading a number,
                    // stuff it in the buffer, otherwise ignore it
                    if(wbufpos == 0) { 
                        wbuf[wbufpos++] = inst;
                    }
                    break;
                case '.':
                    // if this is the first dot we've seen and there's space in the buffer
                    // append it, otherwise ignore it
                    // 0....1.000 -> 0.1000 
                    if(!(flags & RPN_POINT_FLAG) && wbufpos <= RPN_WBUFLEN) { 
                        wbuf[wbufpos++] = inst; 
                        flags |= RPN_POINT_FLAG; 
                    }
                    break;
                case ')':
                    // clear the flags
                    flags = 0;
                    // parse number
                    wbuf[wbufpos] = '\0';
                    temp_a.f = fix16_from_str(wbuf);
                    rpn_push(temp_a); // push the number on the stack
                    break;
                default:
                    if(ISDIGIT(inst) && wbufpos <= RPN_WBUFLEN) {
                        if(!(flags & RPN_READ_FLAG)) { 
                            wbufpos = 0; 
                            flags |= RPN_READ_FLAG;
                        }
                        wbuf[wbufpos++] = inst;
                    }
                    else {
                        flags = 0;
                        wbuf[wbufpos] = '\0';
                        temp_a.f = fix16_from_str(wbuf);
                        rpn_push(temp_a); // push the number on the stack
                        pc--; // wind the program counter back so this byte goes through the interpreter next pass                       
                    }
                    break;
            }
        }
        else {
            switch(inst) {
                /* data access bytecodes */
                case 'E': // pop item, push env[item]
                    temp_a = rpn_pop(); // pop register index
                    temp_b.f = fix16_from_int(env[temp_a.i]);
                    rpn_push(temp_b); // push register value
                    break;
                case 'R': // pop item, push scratch[item]
                    temp_a = rpn_pop();
                    temp_b.f = scratch[temp_a.f];
                    rpn_push(temp_b);
                    break;
                case 'W': // pop index and value, set scratch[index] = value
                    temp_a = rpn_pop();
                    temp_b = rpn_pop();
                    scratch[temp_a.f] = temp_b.f;
                    break;
                case 'O': // pop item, set out[outp] = item, increment outp
                          // no effect if outp >= 4 (out buffer size)
                    if(outp < PROG_OUTBUF_SIZE) {
                        temp_a = rpn_pop();
                        out[outp] = temp_a.f;
                        outp++;
                    }
                    break;
                case 'M':
                    // stub
                    break;
                case 'P':
                    // stub
                    break;
                /* conditional and related */
                case 'J': // pop stack and add to the program counter, incrementing an iteration counter if jumping backwards
                {
                    int8_t jump;
                    temp_a.f = fix16_floor(rpn_pop().f);
                    jump = (int8_t)fix16_to_int( fix16_add(temp_a.f, fix16_from_int(pc)) );
                    if(jump < pc) {
                        loopctr++;
                    }
                    if((loopctr < RPN_MAX_ITER) && (jump >= 0) && (jump <= PROG_SIZE)) {
                        pc = jump;
                    }
                }
                    break;
                case '?': // pop item, if item == 0 fast forward until we hit a ':'
                    temp_a = rpn_pop();
                    if(temp_a.i == 0) {
                        for(; program[pc+1] != ':' && program[pc+1] != '\0'; pc++);
                    }
                    break;
                case ':': // fast forward until we hit a '!'
                    for(; program[pc] != '!' && program[pc] != '\0'; pc++);
                    break;
                case '=': // pop a and b, if a == b push one, if not push zero
                    temp_a = rpn_pop(); 
                    temp_b.i = (rpn_pop()).f == temp_a.f ? 1 : 0;
                    rpn_push(temp_b);              
                    break;
                case '<': // less than
                    temp_a = rpn_pop(); 
                    temp_b.i = (rpn_pop()).f < temp_a.f ? 1 : 0;
                    rpn_push(temp_b);            
                    break;
                case '>': // greater than
                    temp_a = rpn_pop(); 
                    temp_b.i = (rpn_pop()).f > temp_a.f ? 1 : 0;
                    rpn_push(temp_b);             
                    break;
                case 'l': // less or equal to
                    temp_a = rpn_pop(); 
                    temp_b.i = (rpn_pop()).f <= temp_a.f ? 1 : 0;
                    rpn_push(temp_b);            
                    break;
                case 'g': // greater or equal to
                    temp_a = rpn_pop(); 
                    temp_b.i = (rpn_pop()).f >= temp_a.f ? 1 : 0;
                    rpn_push(temp_b);            
                    break;
                case 'N': // not equal to
                    temp_a = rpn_pop(); 
                    temp_b.i = (rpn_pop()).f != temp_a.f ? 1 : 0;
                    rpn_push(temp_b);            
                    break;
                case '&': // logical and
                    temp_a = rpn_pop(); 
                    temp_b.i = (rpn_pop()).f && temp_a.f ? 1 : 0;
                    rpn_push(temp_b);                        
                    break;
                case '|': // logical or
                    temp_a = rpn_pop(); 
                    temp_b.i = (rpn_pop()).f || temp_a.f ? 1 : 0;
                    rpn_push(temp_b);     
                    break;
                case '@': // logical not
                    temp_a = rpn_pop(); 
                    temp_b.i = !(temp_a.f > 0) ? 1 : 0;
                    rpn_push(temp_b);     
                    break;
                /* generative */
                case 'x': // random fix 0 .. 1
                    //temp_a.f = fix16_from_float(rand() / (float)RAND_MAX);
                    temp_a.f = fix16_sdiv(fix16_from_int(rand()), F16(RAND_MAX));
                    rpn_push(temp_a);            
                    break;                
                /* math operations */
                case '+':
                    temp_a = rpn_pop();
                    temp_b.f = fix16_sadd((rpn_pop()).f, temp_a.f);
                    rpn_push(temp_b);
                    break;
                case '-':                                
                    temp_a = rpn_pop();
                    temp_b.f = fix16_ssub((rpn_pop()).f, temp_a.f);
                    rpn_push(temp_b);
                    break;
                case '*':
                    temp_a = rpn_pop();
                    temp_b.f = fix16_smul((rpn_pop()).f, temp_a.f);
                    rpn_push(temp_b);
                    break;
                case '/':
                    temp_a = rpn_pop();
                    temp_b.f = fix16_sdiv((rpn_pop()).f, temp_a.f);
                    rpn_push(temp_b);
                    break;
                case '%':
                    temp_a = rpn_pop();
                    temp_b.f = fix16_mod((rpn_pop()).f, temp_a.f);
                    rpn_push(temp_b);
                    break;
                case 's':
                    temp_a = rpn_pop();
                    temp_b.i = temp_a.i << (rpn_pop()).i;
                    temp_b.frac = 0;
                    rpn_push(temp_b);
                    break;
                case 'r':
                    temp_a = rpn_pop();
                    temp_b.i = temp_a.i >> (rpn_pop()).i;
                    temp_b.frac = 0;
                    rpn_push(temp_b);
                    break;
                case 'S':
                    temp_a = rpn_pop();
                    temp_b.f = sine(temp_a.i);
                    rpn_push(temp_b);
                default:
                    break;
            }
        }
    } 
    return rpn_stack[rpn_stack_top];
}   