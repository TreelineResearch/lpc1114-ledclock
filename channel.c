#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <fix16.h>

#include "config.h"
#include "common.h"
#include "channel.h"
#include "ws2811.h"
#include "rpn.h"

// ########  #### ##     ## ######## ##       
// ##     ##  ##   ##   ##  ##       ##       
// ##     ##  ##    ## ##   ##       ##       
// ########   ##     ###    ######   ##       
// ##         ##    ## ##   ##       ##       
// ##         ##   ##   ##  ##       ##       
// ##        #### ##     ## ######## ######## 

/*
    \brief Generate a 9-segment character and write the pixels to a buffer
    \param buf The pixel buffer to write to
    \param c The ASCII character to write
    \param offset Start point inside the buffer
    \param fg 24-bit foreground color (0xXXRRBBGG), X = padding
    \param bg 24-bit background color
    
    Depends on CHARGEN table, arranged as a 'dense' array of 9-bit patterns starting
    with CHARGEN[0] = <0 pattern> and ending with CHARGEN[35] = <Z pattern>.
    Additional symbols can be mapped onto the end if needed, 
    unknown symbols are emitted as blanks, case is ignored.
    
    This function does not stream out the buffer on its own, it just fills a buffer
    with 24 bit pixels appropriate for ws2811_send()
*/
void seg_putc(pixel_t buf[], uint8_t c, uint8_t offset, uint32_t fg, uint32_t bg) {
    uint8_t i;
    uint16_t gc;

    // remap ASCII to the chargen table
    if(c >= 48 && c < 58) { gc = CHARGEN[c-48]; } // numbers
    else if(c >= 65 && c < 91) { gc = CHARGEN[(c-65)+10]; } // uppercase
    else if(c >= 97 && c < 123) { gc = CHARGEN[(c-97)+10]; } // lowercase
    else { gc = 0x00; }
    
    for(i = 0; i < 9; i++) {
        if((gc >> i) & 0x01) {
            buf[offset+i].i = fg;
        }
        else {
            buf[offset+i].i = bg;
        }
    }
}


/*
    \brief Convert a string to segment-pixels and write them to a buffer
    \param buf The pixel buffer to write to (must be long enough to hold strlen(s)*4 bytes)
    \param s The null-terminated ASCII string to write
    \param fg 24-bit foreground color (0xXXRRBBGG), X = padding
    \param bg 24-bit background color
    
    Depends on seg_putc() to do the dirty work
*/

void seg_puts(pixel_t buf[], uint8_t *s, uint32_t fg, uint32_t bg) {
    uint32_t i = 0;
    char c;
    for(i = 0; (c = s[i]) != '\0'; i++) {
        seg_putc(buf, c, i*9, fg, bg);
    }
}

//  ######  ##     ##    ###    ##    ## ##    ## ######## ##       
// ##    ## ##     ##   ## ##   ###   ## ###   ## ##       ##       
// ##       ##     ##  ##   ##  ####  ## ####  ## ##       ##       
// ##       ######### ##     ## ## ## ## ## ## ## ######   ##       
// ##       ##     ## ######### ##  #### ##  #### ##       ##       
// ##    ## ##     ## ##     ## ##   ### ##   ### ##       ##       
//  ######  ##     ## ##     ## ##    ## ##    ## ######## ######## 

/**
	\brief Initialize a channel struct

	\param[out] c channel_t struct to initialize
	\param[in] gpio GPIO masked access pointer
	\param[in] pin GPIO port pin
	\param[in] length Number of pixels on this channel's chain
*/
void channel_init(channel_t *c, GPIO_TYPEDEF *gpio, uint32_t pin, uint8_t length) {
    uint8_t i;
    c->gpio = gpio;
    c->pin = pin;
    c->length = length;
    c->fgcolor = FGCOLOR;
    c->bgcolor = BGCOLOR;
    c->flags = CHAN_FLAG_SEG;
    c->patlen = 0;
    memset(c->data, 0, CHAN_MESSAGE_BUF);
    for(i = 0; i < CHAN_MOD_SLOTS; i++) { 
        c->mods[i] = 0;
    }
    for(i = 0; i < NPROGS; i++) {
    	c->argprog[i] = -1;
    }      
}

/**
	\brief Render a channel
	
	\param[in] c channel_t struct to render
	\param[in,out] env Environment variable table pointer
	\param[in,out] scratch Scratchpad variable table pointer
*/
void channel_render(channel_t *c, uint32_t *envp, fix16_t *scratchp) {
	uint8_t i;
    pixel_t outbuf[c->length];       // pixel buffer
    //memset(&outbuf, 0, c->length * sizeof(pixel_t));
    envp[ENV_NPIXELS] = c->length;
    fix16_t argbuf[PROG_OUTBUF_SIZE] = {0,0,0,0}; // RPN output buffer, input to modulation function
    fix16_t *argp = (fix16_t *)&argbuf;
    
    if(c->flags & CHAN_FLAG_SEG) {
    	// write segment pattern pixels to buffer
    	seg_puts((pixel_t *)&outbuf, (uint8_t *)c->data, c->fgcolor, c->bgcolor);
    }
    else if(c->flags & CHAN_FLAG_PAT) {
    	buf_patfill((pixel_t *)&outbuf, c->length, (uint8_t *)c->data, c->patlen, 0, c->fgcolor, c->bgcolor);
    }
    else if(c->flags & CHAN_FLAG_SOLID_FG) {
        for(i = 0; i < c->length; i++) { outbuf[i] = (pixel_t)c->fgcolor; }
    }
    else if(c->flags & CHAN_FLAG_SOLID_BG) {
        for(i = 0; i < c->length; i++) { outbuf[i] = (pixel_t)c->bgcolor; }
    }

    // run buffer through modulator chain
    for(i = 0; i < CHAN_MOD_SLOTS; i++) {
        if(c->mods[i] != NULL) {
        	int8_t sel_prog = c->argprog[i];
        	if(sel_prog >= 0) {
            	rpn_evaluate((char *)&c->programs[sel_prog], c, envp, scratchp, argp);
            }
            /*
            XXX weird ass bug affecting the 4th program on the second channel
            the first outputted argument is getting lost, but emitting it twice makes it come out
            no idea if it's somewhere here or in rpn.c
            if(c->length == 16) {
                ee_sprintf(outbuf, "ARGS[%u]={%i, %i, %i, %i}\r\n", c->length, fix16_to_int(argbuf[0]), fix16_to_int(argbuf[1]), fix16_to_int(argbuf[2]), fix16_to_int(argbuf[3]));
                uart_puts(outbuf);
            }
            */
            buf_modulate((pixel_t *)&outbuf, c->length, 0, c->mods[i], argp);
        }    
    }

	ws2811_reset(c->gpio, c->pin); 
	ws2811_send(c->gpio, c->pin, outbuf, c->length);    
}

// XXX implement me
void buf_patfill(pixel_t outbuf[],
                 uint8_t outbuflen,
                 uint8_t pbuf[],
                 uint8_t patlen,
                 uint8_t offset,
                 uint32_t fgcolor,
                 uint32_t bgcolor) {
    uint16_t bc = 0, pc = 0; // consumed byte and emitted pixel counters
    uint8_t bi = 0;
    while(pc < outbuflen && bc < patlen) {
        uint8_t patbyte = pbuf[bc];
        for(bi = 0; bi < 8 && pc < outbuflen && bc < patlen; bi++) {
            outbuf[offset+pc].i = ((patbyte >> bi) & 0x01) == 0 ? bgcolor : fgcolor;
            pc++;
        }
        bc++;
    }
}

/**
    \brief Transform a buffer of 24 bit uint32 pixels
    
    \detail Applies modfn to each pixel in buf[], starting at offset and 
    ending after len pixels have been transformed. Pixels are updated in place.
    user_arg is passed through unmodified to the modulation function modfn.
    
    \verbatim
    An example modulation function skeleton:
    uint32_t a_mod_fn(step, steps, pixel, user_arg) {
        // step is the iteration counter, from 0 to len
        // steps is the total number of iterations in this pass, len
        // pixel is the uint32 containing 24 bits of pixel data
        // user_arg is passed in here unmodified from the outside, 
        
        // do something pretty
        
        // return the modified pixel value
    }
    
    An example application of it:
    // transform 16 pixels starting from a_pixel_buf[0]
    buf_modulate(a_pixel_buf, 16, 0, &a_mod_fn, NULL);
    \endverbatim
*/
void buf_modulate(pixel_t buf[], uint8_t len, uint8_t offset, modfn_ptr_t modfn, fix16_t *arglist) {
    uint8_t i;
    for(i = 0; i < len; i++) {
        buf[i+offset] = (*modfn)(i, len, buf[i+offset], arglist);
    }
}