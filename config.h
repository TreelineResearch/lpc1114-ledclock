/** \file config.h
 * \brief Basic configuration of the project
 * \author Freddie Chopin, http://www.freddiechopin.info/
 * \date 2012-01-08
 */

/******************************************************************************
* project: lpc1114_blink_led
* chip: LPC1114
* compiler: arm-none-eabi-gcc (Sourcery CodeBench Lite 2011.09-69) 4.6.1
******************************************************************************/

#ifndef CONFIG_H_
#define CONFIG_H_

#include "hdr/hdr_gpio_masked_access.h"

/*
+=============================================================================+
| global definitions
+=============================================================================+
*/

#define CRYSTAL								18432000	///< quartz crystal resonator which is connected to the chip
#define FREQUENCY							36864000	///< desired target frequency of the core
//#define FREQUENCY							48000000	///< desired target frequency of the core
#define BAUDRATE							19200
#define TICKRATE                            30          ///< SysTick frequency in hertz
#define SEGMENTS                            9           ///< segments per character
#define CHARS                               4           ///< number of N-segment characters
#define NPIXELS                             (SEGMENTS*CHARS) ///< total number of pixels

#define ENV_SIZE 							16 			///< size of the environment table
#define SCRATCH_SIZE 						4 			///< size of the scratch table
#define CONBUF_SIZE							64 			///< console input buffer
#define OUTBUF_SIZE 						(CONBUF_SIZE+8)

#define NCHANNELS							2 			///< number of channels
#define CHAN_MOD_SLOTS 						4 			///< number of modulators per channel
#define CHAN_MESSAGE_BUF 					8          ///< length of channel message buffer

#define NPROGS								8  			///< number of RPN program buffers
#define PROG_SIZE							64 			///< length of each program buffer
#define PROG_OUTBUF_SIZE					4           ///< length of the RPN program output/argument buffer

#define NMODULATORS							8  			///< number of modulator functions

#define CHAN_A_LEN							36
#define CHAN_B_LEN							16
                                               
/// WS2811 drive channel A
#define CHAN_A_GPIO                         LPC_GPIO1
#define CHAN_A_pin                          1
#define CHAN_A                              (1 << CHAN_A_pin)
/// channel B
#define CHAN_B_GPIO                         LPC_GPIO1
#define CHAN_B_pin                          2
#define CHAN_B                              (1 << CHAN_B_pin)

/// "variables" to manipulate the pin directly via GPIO masked access
#define GPIO_TYPEDEF						LPC_GPIO_TypeDef
#define CHAN_A_gma                          gpio_masked_access_t GPIO_MASKED_ACCESS(CHAN_A_GPIO, CHAN_A_pin)
#define CHAN_B_gma                          gpio_masked_access_t GPIO_MASKED_ACCESS(CHAN_B_GPIO, CHAN_B_pin)

#define GMA(GPIO, PIN)                      gpio_masked_access_t GPIO_MASKED_ACCESS(GPIO, PIN)
#define BIT(N)                              (1 << (N))

#define FGCOLOR								0x0000070f
#define BGCOLOR								0x00000000

#define ENV_TICKRATE 0
#define ENV_TICK 1
#define ENV_NPIXELS 2
#define ENV_FRAMETICK 3
#define ENV_RTCSEC 4
#define ENV_RTCMIN 5
#define ENV_RTCHOUR 6
#define ENV_FGCOLOR 7
#define ENV_BGCOLOR 8
#define ENV_CHANLEN 9

#define RTC_FLAG_UNSET 1
#define RTC_FLAG_12HR 2

#define R_BITS 8
#define G_BITS 8
#define B_BITS 8

/*
+=============================================================================+
| strange variables
+=============================================================================+
*/

#define ISDIGIT(c) ((c) - '0' + 0U <= 9U)
#define ISALPHA(c) (((c) | 32) - 'a' + 0U <= 'z' - 'a' + 0U)
#define ISSYMBOL(c) ((c) > 32 && (c) < 48)
#define SAT8(N) (((N > 0xFF) ? 0xFF: N) < 0 ? 0 : N)

/*
+=============================================================================+
| global variables
+=============================================================================+
*/

/*
+=============================================================================+
| global functions' declarations
+=============================================================================+
*/

int ee_sprintf(char *buf, const char *fmt, ...);

/******************************************************************************
* END OF FILE
******************************************************************************/
#endif /* CONFIG_H_ */
