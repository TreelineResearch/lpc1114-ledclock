#ifndef __APP_H__
#define __APP_H__
#include <stdint.h>

typedef enum {IDLE, STARTCMD, ENDCMD, FAIL} state_t;

void app_start(void);
void dump(void);
uint8_t read_int(char *, int32_t *, char);
void read_cmd(char *);

#endif

