#ifndef __MODULATOR_H__
#define __MODULATOR_H__
#include <fix16.h>
#include "common.h"

#define MOD_FN(_NAME_) extern pixel_t _NAME_(uint8_t step, uint8_t steps, pixel_t pixel, fix16_t *a)

MOD_FN(mod_hsl);
MOD_FN(mod_flicker);
MOD_FN(mod_gaussian);
MOD_FN(mod_raw);
MOD_FN(mod_mul);

#endif