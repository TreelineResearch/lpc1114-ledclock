import cgi
import cgitb
import json
import serial
from time import sleep

SERIALPORT = '/dev/ttyS1'
SERIALBAUD = 19200

data = cgi.FieldStorage();
op = data.getvalue('op')
reqdata = data.getvalue('json')

# op = 'save'
# json = 'dicksdicksdicks'
# op = 'upload'
# reqdata = '["load 0 3 O 5E 60 / 4E 60 / 60 / + 360 * O","load 1 0.6O 0O","load 2 1O 3E 1.5*O 8O 0.5O","load 3 1.5O 0O","mods 0 1 2 5 0 ","prgs 0 0 1 2 -1 ","mods 1 2 0 0 0 ","prgs 1 3 -1 -1 -1 "]'
# op = 'load'
# reqdata = ''

def save(data):
	with open("/home/cubie/frontend/data/savedstates.json", "w+") as f:
		f.write(data)
		f.seek(0)
		savedstates = f.read()
	
	return savedstates

def load(data):
	with open("/home/cubie/frontend/data/savedstates.json", "r") as f:
		savedstates = f.read() or ""
		
	return savedstates
	
def upload(data):
	script = json.loads(data)
	port = serial.Serial(SERIALPORT, SERIALBAUD, timeout=1, xonxoff=True)
	for line in script:
		print line
		print port.write(str(line) + "\r")
		# sleep(1.5) # little delay so we don't overrun the buffer while the last command is processing
		while port.read():
			pass
	port.close()
	return script[0]

routes = {'save':save,
		  'load':load,
		  'upload':upload}
	
print "Content-Type: application/json"
print

if(op in routes):
	print routes[op](reqdata)
else:
	print "Unknown operation"