import json
import serial

SERIALPORT = '/dev/ttyS1'
SERIALBAUD = 19200

script = ["ATST",
          "ATST23,23,23",
          "ATC"]

port = serial.Serial(SERIALPORT, SERIALBAUD, timeout=1, xonxoff=True)

rxlines = [];

for line in script:
	rchars = ""
	port.flushInput()
	line = str(line) + "\r"
	wchars = port.write(line)

	while True:
		rc = port.read()
		if rc:
			rchars = rchars + rc
		else:
			break

	rxlines.append({"tx":line, "rx":rchars})

print rxlines

port.close()


