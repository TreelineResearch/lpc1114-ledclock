#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "inc/LPC11xx.h"
#include "inc/core_cm0.h"
#include "hdr/hdr_syscon.h"

#include "app.h"
#include "common.h"

//  ######   ##        #######  ########     ###    ##        ######  
// ##    ##  ##       ##     ## ##     ##   ## ##   ##       ##    ## 
// ##        ##       ##     ## ##     ##  ##   ##  ##       ##       
// ##   #### ##       ##     ## ########  ##     ## ##        ######  
// ##    ##  ##       ##     ## ##     ## ######### ##             ## 
// ##    ##  ##       ##     ## ##     ## ##     ## ##       ##    ## 
//  ######   ########  #######  ########  ##     ## ########  ######  

static rtc_t g_rtc;                  ///< rtc state
static console_t g_cons;             ///< console state
static char outbuf[OUTBUF_SIZE];     ///< A buffer for printf and shit
static uint8_t uart_checksum;        ///< UART TX checksum

console_t *get_console_ptr(void) {
    return (console_t *)&g_cons;
}

rtc_t *get_rtc_ptr(void) {
    return (rtc_t *)&g_rtc;
}

char *get_outbuf_ptr(void) {
    outbuf[0] = '\0';
    return (char *)&outbuf;
}

const uint8_t  sine_wave[360] = {
    0x80,0x82,0x84,0x86,0x88,0x8b,0x8d,0x8f,
    0x91,0x93,0x96,0x98,0x9a,0x9c,0x9e,0xa0,
    0xa3,0xa5,0xa7,0xa9,0xab,0xad,0xaf,0xb1,
    0xb3,0xb5,0xb7,0xb9,0xbb,0xbd,0xbf,0xc1,
    0xc3,0xc5,0xc7,0xc9,0xca,0xcc,0xce,0xd0,
    0xd1,0xd3,0xd5,0xd6,0xd8,0xda,0xdb,0xdd,
    0xde,0xe0,0xe1,0xe3,0xe4,0xe5,0xe7,0xe8,
    0xe9,0xea,0xec,0xed,0xee,0xef,0xf0,0xf1,
    0xf2,0xf3,0xf4,0xf5,0xf6,0xf7,0xf7,0xf8,
    0xf9,0xf9,0xfa,0xfb,0xfb,0xfc,0xfc,0xfd,
    0xfd,0xfd,0xfe,0xfe,0xfe,0xff,0xff,0xff,
    0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
    0xfe,0xfe,0xfe,0xfd,0xfd,0xfd,0xfc,0xfc,
    0xfb,0xfb,0xfa,0xf9,0xf9,0xf8,0xf7,0xf7,
    0xf6,0xf5,0xf4,0xf3,0xf2,0xf1,0xf0,0xef,
    0xee,0xed,0xec,0xea,0xe9,0xe8,0xe7,0xe5,
    0xe4,0xe3,0xe1,0xe0,0xde,0xdd,0xdb,0xda,
    0xd8,0xd6,0xd5,0xd3,0xd1,0xd0,0xce,0xcc,
    0xca,0xc9,0xc7,0xc5,0xc3,0xc1,0xbf,0xbd,
    0xbb,0xb9,0xb7,0xb5,0xb3,0xb1,0xaf,0xad,
    0xab,0xa9,0xa7,0xa5,0xa3,0xa0,0x9e,0x9c,
    0x9a,0x98,0x96,0x93,0x91,0x8f,0x8d,0x8b,
    0x88,0x86,0x84,0x82,0x80,0x7d,0x7b,0x79,
    0x77,0x74,0x72,0x70,0x6e,0x6c,0x69,0x67,
    0x65,0x63,0x61,0x5f,0x5c,0x5a,0x58,0x56,
    0x54,0x52,0x50,0x4e,0x4c,0x4a,0x48,0x46,
    0x44,0x42,0x40,0x3e,0x3c,0x3a,0x38,0x36,
    0x35,0x33,0x31,0x2f,0x2e,0x2c,0x2a,0x29,
    0x27,0x25,0x24,0x22,0x21,0x1f,0x1e,0x1c,
    0x1b,0x1a,0x18,0x17,0x16,0x15,0x13,0x12,
    0x11,0x10,0xf,0xe,0xd,0xc,0xb,0xa,
    0x9,0x8,0x8,0x7,0x6,0x6,0x5,0x4,
    0x4,0x3,0x3,0x2,0x2,0x2,0x1,0x1,
    0x1,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
    0x0,0x0,0x0,0x0,0x1,0x1,0x1,0x2,
    0x2,0x2,0x3,0x3,0x4,0x4,0x5,0x6,
    0x6,0x7,0x8,0x8,0x9,0xa,0xb,0xc,
    0xd,0xe,0xf,0x10,0x11,0x12,0x13,0x15,
    0x16,0x17,0x18,0x1a,0x1b,0x1c,0x1e,0x1f,
    0x21,0x22,0x24,0x25,0x27,0x29,0x2a,0x2c,
    0x2e,0x2f,0x31,0x33,0x35,0x36,0x38,0x3a,
    0x3c,0x3e,0x40,0x42,0x44,0x46,0x48,0x4a,
    0x4c,0x4e,0x50,0x52,0x54,0x56,0x58,0x5a,
    0x5c,0x5f,0x61,0x63,0x65,0x67,0x69,0x6c,
    0x6e,0x70,0x72,0x74,0x77,0x79,0x7b,0x7d
};

fix16_t sine(uint16_t degrees) {
    degrees %= 360;
    return fix16_div(fix16_from_int(sine_wave[degrees]), F16(255));
}

// ########  ########  ######  
// ##     ##    ##    ##    ## 
// ##     ##    ##    ##       
// ########     ##    ##       
// ##   ##      ##    ##       
// ##    ##     ##    ##    ## 
// ##     ##    ##     ######  

void rtc_init(rtc_t *rtc) {
    rtc->flags = RTC_FLAG_UNSET;
    rtc->seconds = 0;
    rtc->minutes = 0;
    rtc->hours = 0;
}

uint32_t rtc_set(rtc_t *rtc, uint8_t h, uint8_t m, uint8_t s) {
    if(h < 24 && m < 60 && s < 60) {
        rtc->hours = h;
        rtc->minutes = m;
        rtc-> seconds = s;
        rtc->flags &= ~RTC_FLAG_UNSET;
        return 0;
    }
    else {
        return 1;
    }
}

void rtc_tick(rtc_t *s) {
    s->seconds++;
    if(s->seconds > 59) {
        s->seconds = 0;
        s->minutes++;
        if(s->minutes > 59) {
            s->minutes = 0;
            s->hours++;
            if(s->hours > 23) {
                s->hours = 0;
            }
        }
    }
}

// ######## ##    ## ##     ## 
// ##       ###   ## ##     ## 
// ##       ####  ## ##     ## 
// ######   ## ## ## ##     ## 
// ##       ##  ####  ##   ##  
// ##       ##   ###   ## ##   
// ######## ##    ##    ###    

/**
    \brief Update environment variables
*/

void update_environment(uint32_t e[], uint32_t tick, rtc_t *rtc) {
    e[ENV_TICKRATE] = TICKRATE;
    e[ENV_TICK] = tick;
    e[ENV_FRAMETICK] = (tick % TICKRATE);
    e[ENV_NPIXELS] = 0;
    e[ENV_RTCSEC] = rtc->seconds;
    e[ENV_RTCMIN] = rtc->minutes;
    e[ENV_RTCHOUR] = rtc->hours;
    e[ENV_FGCOLOR] = FGCOLOR;
    e[ENV_BGCOLOR] = BGCOLOR;
}


//  ######   #######  ##    ##  ######   #######  ##       ######## 
// ##    ## ##     ## ###   ## ##    ## ##     ## ##       ##       
// ##       ##     ## ####  ## ##       ##     ## ##       ##       
// ##       ##     ## ## ## ##  ######  ##     ## ##       ######   
// ##       ##     ## ##  ####       ## ##     ## ##       ##       
// ##    ## ##     ## ##   ### ##    ## ##     ## ##       ##       
//  ######   #######  ##    ##  ######   #######  ######## ######## 

void console_init(console_t *cons) {
    cons->ipos = 0;
    cons->state = WAIT;
#ifdef DOUBLE_BUFFER_UART
    cons->inbuf = cons->buf_a;
    cons->cmdbuf = cons->buf_b;
#else
    cons->inbuf = cons->buf_a;
    cons->cmdbuf = cons->buf_a;
#endif    
}

void handle_console_line(console_t *cons) {
    switch(cons->state) {
        case LINE_RDY:
            LPC_UART->THR = XOFF;
            read_cmd(cons->cmdbuf);
            //memset(cons->cmdbuf, 0, CONBUF_SIZE);
            uart_puts("\r\n> ");
            LPC_UART->THR = XON;
            cons->state = WAIT;
            break;
        case BUF_OVF:
            LPC_UART->THR = XOFF;
            uart_puts("\r\nSLOW DOWN!\r\n");
            //memset(cons->inbuf, 0, CONBUF_SIZE);
            LPC_UART->THR = XON;
            cons->state = WAIT;
            break;
        case RESET:
            uart_puts("\r\n> ");
            LPC_UART->THR = XON;
            cons->state = WAIT;
            break;
        default:
            break;            
    }
}

// ##     ##    ###    ########  ######## 
// ##     ##   ## ##   ##     ##    ##    
// ##     ##  ##   ##  ##     ##    ##    
// ##     ## ##     ## ########     ##    
// ##     ## ######### ##   ##      ##    
// ##     ## ##     ## ##    ##     ##    
//  #######  ##     ## ##     ##    ##    
/*
    \brief Initialize the LPC UART and associated IOs
    
    RTFM (ch. 13 in UM10398), no sense going deep into the registers here.
    Fractional divider isn't used because we can evenly divide 36.864MHz into
    the (16*)19200 baud rate we're using. 
    (...and that's why I keep 18.432MHz xtals on hand.)
*/
void uart_init(void) {
    LPC_IOCON->PIO1_6 |= 1; // enable rxd
    LPC_IOCON->PIO1_7 |= 1; // enable txd
    LPC_SYSCON->SYSAHBCLKCTRL |= SYSAHBCLKCTRL_UART; // enable UART clock
    LPC_SYSCON->UARTCLKDIV = 1; // no clock prescale
    LPC_UART->LCR = 0x83; // set latch access bit and 8/N/1
    LPC_UART->DLL = FREQUENCY / (16 * BAUDRATE); // divisor = 36.8MHz / (16 * BAUDRATE)
    LPC_UART->DLM = 0;  // don't need the high byte of the divisor
    LPC_UART->LCR = 0x03; // clear latch access bit
    LPC_UART->IER = 0x01; // enable the rx data available interrupt
    LPC_UART->FCR = 0xC7; // reset fifos and enable, set RXTL to 14 bytes
    LPC_UART->TER = 0x80; // turn on transmitter
}

uint8_t uart_reset_checksum(void) {
    uint8_t old_checksum = uart_checksum;
    uart_checksum = 0;
    return old_checksum;
}

/*
    \brief Write a string to the LPC UART
    \param s The null-terminated string to write
    
    The LPC UART has a 16 byte FIFO, so we'll try to be a bit efficient 
    by filling it up and waiting for it to empty before sending another chunk
    rather than sending a byte at a time, since it can be transmitting all the 
    while we're stuffing its buffer. 
*/
void uart_puts(char *s) {
    uint32_t i = 0, bc = 0;
    uint32_t bufs = (strlen(s) >> 4) + 1; // number of buffer fills needed for the message
    char c;
    // enable tx
    LPC_UART->TER = 0x80;
    while(bufs--) {
        // if there's stuff in the buffer wait for it to empty out 
        while(!(LPC_UART->LSR >> 6 & 0x01));
        // fill up the buffer
        c = 1;
        for(i = 0; (i < 16) && c != '\0'; i++) {
            c = s[i + (bc << 4)];
            LPC_UART->THR = c;
            uart_checksum ^= c;   
        }
        bc++;
    }
}

void uart_poll(void) {
    console_t *cons = (console_t *)get_console_ptr(); 
    char rc;                                 // rx char

    while(LPC_UART->LSR & 0x01) {            // while there's data in the buffer
        rc = LPC_UART->RBR;                  // read a character from the rx buffer
        if(cons->ipos >= CONBUF_SIZE) {      // check for line buffer full
            LPC_UART->THR = XOFF;
            cons->inbuf[CONBUF_SIZE] = '\0'; // terminate
            cons->ipos = 0;                  // reset buffer counter
            cons->state = BUF_OVF;           // signal overflow
            break; 
        }
        else if(cons->state == WAIT && (rc == '\r' || rc == '\n')) { // if rc is a terminator char we have a line
            LPC_UART->THR = XOFF;                                    // pray the host stops sending data
            cons->inbuf[cons->ipos] = '\0';                          // terminate
            cons->ipos = 0;                                          // reset counter
            
#ifdef DOUBLE_BUFFER_UART                    // swap input buffers if we can afford two buffers
            cons->inbuf = (cons->inbuf == &(cons->buf_a)) ? &(cons->buf_b) : &(cons->buf_a);
            cons->cmdbuf = (cons->cmdbuf == &(cons->buf_a)) ? &(cons->buf_b) : &(cons->buf_a);
#endif

            cons->state = LINE_RDY;                                  // signal we have a line
            break;
        } else if(rc == 0x03) {
            cons->inbuf[0] = '\0'; // terminate
            cons->ipos = 0;                  // reset buffer counter
            cons->state = RESET;           // signal overflow
            break;             
        }
        else {
            LPC_UART->THR = rc;                                      // otherwise echo rc and append it to the input buffer
            cons->inbuf[cons->ipos++] = rc;
            cons->inbuf[cons->ipos] = '\0';
        }
    }
}