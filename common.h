#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdint.h>
#include <fix16.h>
#include "inc/LPC11xx.h"
#include "config.h"

#define XON 0x11
#define XOFF 0x13

/* Type declares */
typedef struct {
	uint8_t flags;
    uint8_t hours;
    uint8_t minutes;
    uint8_t seconds;
} rtc_t;

typedef enum {WAIT, RESET, LINE_RDY, BUF_OVF} cons_state_t;

typedef struct {
    cons_state_t state;
    uint8_t ipos;
    char *inbuf;
    char *cmdbuf;
    char buf_a[CONBUF_SIZE+1];
#ifdef DOUBLE_BUFFER_UART
    char buf_b[CONBUF_SIZE+1];
#endif
} console_t;

typedef uint32_t pixel_size_t;

typedef union pixel {
    pixel_size_t i;
    struct {
        pixel_size_t r : R_BITS;
        pixel_size_t g : G_BITS;
        pixel_size_t b : B_BITS;
    };
} pixel_t;

/// Typedef for modulation function pointer calls
typedef pixel_t (*modfn_ptr_t) (uint8_t, uint8_t, pixel_t, fix16_t *);

/**
    \struct channel_t
    \brief channel_t state container
*/ 
typedef struct {
    GPIO_TYPEDEF *gpio;                 ///< GPIO masked access pointer
    uint32_t pin;                       ///< GPIO port pin

    uint8_t flags;                      ///< XXX Flags, TBD
    uint8_t length;                     ///< Length of channel in pixels
    uint8_t patlen;                     ///< Length of pattern in bits

    modfn_ptr_t mods[CHAN_MOD_SLOTS];   ///< Modulation function pointer table
    char (*programs)[PROG_SIZE+1];        ///< Modulation argument programs (ptr to shared data)

    uint32_t fgcolor;                   ///< channel_t foreground (on) color 0xXXRRBBGG
    uint32_t bgcolor;                   ///< channel_t background (off) color

    int8_t argprog[NPROGS];             ///< Modulator argument program index
    uint8_t pattern[(NPIXELS/8)+1];     ///< XXX Bitmap pattern buffer TBD
    uint8_t data[CHAN_MESSAGE_BUF+1];   ///< Message buffer
} channel_t;

/* Function declares */
extern volatile uint32_t *get_tick_ptr(void); // defined in main.c
console_t *get_console_ptr(void);
rtc_t *get_rtc_ptr(void);
char *get_outbuf_ptr(void);

fix16_t sine(uint16_t degrees);

void update_environment(uint32_t[], uint32_t, rtc_t *); 

void rtc_init(rtc_t *);
uint32_t rtc_set(rtc_t *, uint8_t, uint8_t, uint8_t);
void rtc_tick(rtc_t *);

void console_init(console_t *);

void uart_init(void);
uint8_t uart_reset_checksum(void);
void uart_puts(char *);
void handle_console_line(console_t *);
void uart_poll(void);

#endif