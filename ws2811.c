#include <stdint.h>
#include "inc/LPC11xx.h"
#include "inc/core_cm0.h"
#include "config.h"
#include "hdr/hdr_syscon.h"
#include "channel.h"
#include "ws2811.h"

/*
    \brief Bit-bang a WS2811 bitstream
    \param buf The pixel buffer to send
    \param len The number of pixels to send
    
    This is timed in software and is dependent on the DLYLOOP(N) values, "calibrated" 
    for a 36MHz PCLK. Somewhat surprisingly, this appears to work up
    to 50MHz, probably due to GPIO being not particularly fast on the LPC1117.
    Your mileage will vary, bring your oscilloscope and tweak and tune as needed until
    you get in the ballpark of what the datasheet specifies and/or blinkenlights turn on.
    
    Datasheet says R-G-B order, I'm seeing R-B-G. 
    Endian bug here (I'd expect BGR...), or quirky WS2811?
    RBG doesn't bend my mind much so I didn't bother hunting it down. [dealwithit.gif]

    Interrupts will glitch this, but disabling them glitches other stuff. 
    With a 30Hz systick and less than 40 pixels to send we're pretty safe, but
    look here if a string of 150 is sparkling with glitches (they're quite pretty). 
*/

void ws2811_send(LPC_GPIO_TypeDef *gpio, uint32_t pin, pixel_t buf[], uint8_t len) {
    // __disable_irq();
    register int32_t pp, pbitp;
    for(pp = 0; pp < len; pp++) {
        for(pbitp = 23; pbitp >= 0; pbitp--) {
            GMA(gpio, pin) = BIT(pin);
            if((buf[pp].i >> pbitp) & 0x01) {
                DLYLOOP(7);
                GMA(gpio, pin) = 0;
                DLYLOOP(6);
            }
            else {
                DLYLOOP(1);
                GMA(gpio, pin) = 0;
                DLYLOOP(15);
            } 
        }
    }
    // __enable_irq();
}

void ws2811_reset(LPC_GPIO_TypeDef *gpio, uint32_t pin) {
    GMA(gpio, pin) = 0;
    DLYLOOP(1000);
}
