/* 
    \file main.c
    \brief WS2811 segment controller
    \details Startup routines, globals, and interrupt handlers live here.
    \author kmm
    \date 2014

    Low-level routines and build scripts based on 'lpc1114_blink_led' sample 
    project skeleton by Freddie Chopin, http://www.freddiechopin.info/. 
 */
 
/*
+=============================================================================+
| includes
+=============================================================================+
*/

#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fix16.h>

#include "inc/LPC11xx.h"
#include "inc/core_cm0.h"
#include "config.h"
#include "hdr/hdr_syscon.h"

#include "ws2811.h"
#include "common.h"
#include "app.h"

/*
+=============================================================================+
| global variables and accessors
+=============================================================================+
*/
static volatile uint32_t g_tick = 0; ///< global tick counter, incremented at 30hz by the systick interrupt

volatile uint32_t *get_tick_ptr(void) {
    return (volatile uint32_t *)&g_tick;
}
/*
+=============================================================================+
| local functions' declarations
+=============================================================================+
*/
static void flash_access_time(uint32_t frequency);
static uint32_t pll_start(uint32_t crystal, uint32_t frequency);
static void system_init(void);
void timer0_init(void);
void sys_tick_init(void);
/*
+=============================================================================+
| global functions
+=============================================================================+
*/

/* _sbrk swiped from http://www.pittnerovi.com/jiri/hobby/electronics/lpc1114/ */
caddr_t  _sbrk (int32_t incr)
{
	extern uint32_t __heap_start, __heap_end;
	static unsigned char *heap = NULL;
	void *prev_heap;
	void *heap_end = &__heap_end;

    if (heap == NULL) prev_heap = heap = (unsigned char *)&__heap_start; 
 
    if ((prev_heap + incr) < heap_end)
	{
      	prev_heap = heap;
      	heap += incr;
	}
    else 
    	return NULL;
	
	return (caddr_t) prev_heap;
}

/*
    \brief Initialize the SysTick timer
    
*/
void sys_tick_init(void) {
    SysTick_Config(FREQUENCY / TICKRATE);
}

/*
    \brief main code block
    
    \details Initializes system and invokes main app run loop
*/

int main(void)
{
	pll_start(CRYSTAL, FREQUENCY);			// start the PLL
	system_init();							// initialize other necessary elements
	uart_init();                            // initialize the UART
    sys_tick_init();
    
    // enable interrupts
    NVIC_EnableIRQ(UART_IRQn);
    __enable_irq();
    
    // start the application
    app_start();
    
	// app shouldn't return unless it crashes or something
	char outbuf[16];
	while(1) { 	
		ee_sprintf(outbuf, "CRASH\r\n"); 
    	uart_puts(outbuf);
	}
}

/*
+=============================================================================+
| local functions
+=============================================================================+
*/

/*------------------------------------------------------------------------*//**
* \brief Configures flash access time.
* \details Configures flash access time which allows the chip to run at higher
* speeds.
*
* \param [in] frequency defines the target frequency of the core
*//*-------------------------------------------------------------------------*/

static void flash_access_time(uint32_t frequency)
{
	uint32_t access_time, flashcfg_register;

	if (frequency < 20000000ul)				// 1 system clock for core speed below 20MHz
		access_time = FLASHCFG_FLASHTIM_1CLK;
	else if (frequency < 40000000ul)		// 2 system clocks for core speed between 20MHz and 40MHz
		access_time = FLASHCFG_FLASHTIM_2CLK;
	else									// 3 system clocks for core speed over 40MHz
		access_time = FLASHCFG_FLASHTIM_3CLK;

	// do not modify reserved bits in FLASHCFG register
	flashcfg_register = FLASHCFG;			// read register
	flashcfg_register &= ~(FLASHCFG_FLASHTIM_mask << FLASHCFG_FLASHTIM_bit);	// mask the FLASHTIM field
	flashcfg_register |= access_time << FLASHCFG_FLASHTIM_bit;	// use new FLASHTIM value
	FLASHCFG = flashcfg_register;			// save the new value back to the register
}

/*------------------------------------------------------------------------*//**
* \brief Starts the PLL.
* \details Configure and enable PLL to achieve some frequency with some
* crystal. Before the speed change flash access time is configured via
* flash_access_time(). Main oscillator is configured and started. PLL
* parameters m and p are based on function parameters. The PLL is configured,
* started and selected as the main clock. AHB clock divider is set to 1.
*
* \param [in] crystal is the frequency of the crystal resonator connected to
* the LPC1114 chip.
* \param [in] frequency is the desired target frequency after enabling the PLL
*
* \return real frequency that was set
*//*-------------------------------------------------------------------------*/

static uint32_t pll_start(uint32_t crystal, uint32_t frequency)
{
	uint32_t m, p = 0, fcco;

	flash_access_time(frequency);			// configure flash access time first

	// SYSOSCCTRL_FREQRANGE should be 0 for crystals in range 1 - 20MHz
	// SYSOSCCTRL_FREQRANGE should be 1 for crystals in range 15 - 25MHz
	if (crystal < 17500000)					// divide the ranges on 17.5MHz then
		LPC_SYSCON->SYSOSCCTRL = 0;			// "lower speed" crystals
	else
		LPC_SYSCON->SYSOSCCTRL = SYSOSCCTRL_FREQRANGE;	// "higher speed" crystals

	LPC_SYSCON->PDRUNCFG &= ~PDRUNCFG_SYSOSC_PD;	// power-up main oscillator

	LPC_SYSCON->SYSPLLCLKSEL = SYSPLLCLKSEL_SEL_SYSOSC;	// select main oscillator as the input clock for PLL
	LPC_SYSCON->SYSPLLCLKUEN = 0;			// confirm the change of PLL input clock by toggling the...
	LPC_SYSCON->SYSPLLCLKUEN = SYSPLLUEN_ENA;	// ...ENA bit in LPC_SYSCON->SYSPLLCLKUEN register

	// calculate PLL parameters
	m = frequency / crystal;				// M is the PLL multiplier
	fcco = m * crystal * 2;					// FCCO is the internal PLL frequency

	frequency = crystal * m;

	while (fcco < 156000000)
	{
		fcco *= 2;
		p++;								// find P which gives FCCO in the allowed range (over 156MHz)
	}

	LPC_SYSCON->SYSPLLCTRL = ((m - 1) << SYSPLLCTRL_MSEL_bit) | (p << SYSPLLCTRL_PSEL_bit);	// configure PLL
	LPC_SYSCON->PDRUNCFG &= ~PDRUNCFG_SYSPLL_PD; // power-up PLL

	while (!(LPC_SYSCON->SYSPLLSTAT & SYSPLLSTAT_LOCK));	// wait for PLL lock

	LPC_SYSCON->MAINCLKSEL = MAINCLKSEL_SEL_PLLOUT;	// select PLL output as the main clock
	LPC_SYSCON->MAINCLKUEN = 0;				// confirm the change of main clock by toggling the...
	LPC_SYSCON->MAINCLKUEN = MAINCLKUEN_ENA;	// ...ENA bit in LPC_SYSCON->MAINCLKUEN register

	LPC_SYSCON->SYSAHBCLKDIV = 1;			// set AHB clock divider to 1

	return frequency;
}

/*------------------------------------------------------------------------*//**
* \brief Initializes system.
* \details Enables clock for IO configuration block.
*//*-------------------------------------------------------------------------*/

static void system_init(void)
{
	LPC_SYSCON->SYSAHBCLKCTRL |= SYSAHBCLKCTRL_IOCON;	// enable clock for IO configuration block
    
    LPC_IOCON->R_PIO1_1 = 0xD1; // PIO1_1, gpio function, pullup enabled, analog disabled
    LPC_IOCON->R_PIO1_2 = 0x91; // PIO1_2, gpio function, pullup enabled, analog disabled

    CHAN_A_GPIO->DIR |= CHAN_A; // PIO1_1
    CHAN_B_GPIO->DIR |= CHAN_B; // PIO1_2
}

/*
+=============================================================================+
| ISRs
+=============================================================================+
*/

/*
    \brief SysTick interrupt handler
*/
void SysTick_Handler(void) {
    g_tick++;
}

/*
    \brief UART receive interrupt handler
*/
void UART_IRQHandler(void) {
	uart_poll();
}
