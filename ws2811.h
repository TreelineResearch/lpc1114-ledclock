#ifndef __WS2811_H__
#define __WS2811_H__

#include "inc/LPC11xx.h"
#include "channel.h"

#ifndef __clang__ // stub out the asm for test building with clang (it has much nicer errors/warnings than gcc)
#define DLYLOOP(val) \
    __asm__ __volatile__ ( \
        "mov r5, #0\n\t" \
        "1:\n\t" \
        "add r5, #1\n\t" \
        "cmp r5, %0\n\t" \
        "bne 1b\n\t" \
        : \
        : "l" (val) \
        : "r5" \
    );
#else
#define DLYLOOP(val) ; 
#endif

void ws2811_reset(LPC_GPIO_TypeDef *gpio, uint32_t pin);
void ws2811_send(LPC_GPIO_TypeDef *gpio, uint32_t pin, pixel_t *buf, uint8_t len);

#endif
