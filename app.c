/** \file app.c
    \brief Main application implementation
    \details Main application implementation
    \author K McGinley <kmm at rmlabs dot net>
    \date 2014-07-28
*/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <fix16.h>

#include "inc/LPC11xx.h"
#include "inc/core_cm0.h"
#include "config.h"
#include "hdr/hdr_syscon.h"

#include "common.h"
#include "ws2811.h"
#include "channel.h"
#include "modulator.h"
#include "rpn.h"
#include "app.h"

// ##     ##    ###    ########   ######  
// ##     ##   ## ##   ##     ## ##    ## 
// ##     ##  ##   ##  ##     ## ##       
// ##     ## ##     ## ########   ######  
//  ##   ##  ######### ##   ##         ## 
//   ## ##   ##     ## ##    ##  ##    ## 
//    ###    ##     ## ##     ##  ######  

/// Channel state structs
channel_t channels[NCHANNELS];

/// Modulator function pointer table
modfn_ptr_t modulators[] = { 
    0, ///< null modulator
    (modfn_ptr_t)&mod_hsl,
    (modfn_ptr_t)&mod_flicker,
    (modfn_ptr_t)&mod_gaussian,
    (modfn_ptr_t)&mod_raw,
    (modfn_ptr_t)&mod_mul,
    0,
    0 
};

char programs[NPROGS][PROG_SIZE];
uint32_t environment[ENV_SIZE];     ///< Precalculated environment variable table
fix16_t scratchpad[SCRATCH_SIZE];  ///< Scratchpad variable table
static int8_t frameprogram = -1;

// ########  ##     ## ##    ##    ##        #######   #######  ########  
// ##     ## ##     ## ###   ##    ##       ##     ## ##     ## ##     ## 
// ##     ## ##     ## ####  ##    ##       ##     ## ##     ## ##     ## 
// ########  ##     ## ## ## ##    ##       ##     ## ##     ## ########  
// ##   ##   ##     ## ##  ####    ##       ##     ## ##     ## ##        
// ##    ##  ##     ## ##   ###    ##       ##     ## ##     ## ##        
// ##     ##  #######  ##    ##    ########  #######   #######  ##        

/**
    \brief Start up the main app loop
*/
void app_start(void) {
    uint32_t ltick = 0; // last tick
    volatile uint32_t *tick = (volatile uint32_t *)get_tick_ptr();
    console_t *cons = (console_t *)get_console_ptr(); 
    rtc_t *rtc = (rtc_t *)get_rtc_ptr();
    
    console_init(cons);
    rtc_init(rtc);

    int8_t i = NPROGS;
    while(i-- >= 0) {
        memset(programs[i], 0, PROG_SIZE);
    }
    
    // and the channels
    channel_init(&channels[0], CHAN_A_GPIO, CHAN_A_pin, CHAN_A_LEN);
    channel_init(&channels[1], CHAN_B_GPIO, CHAN_B_pin, CHAN_B_LEN);  
    channels[0].programs = (char (*)[])programs;
    channels[1].programs = (char (*)[])programs;

    while (1)
	{
        // poll console for state changes
        uart_poll();
	    handle_console_line(cons); 

	    if(*tick != ltick) { // fire once at tick delta
	        ltick = *tick;   // save this tick for next delta check
            // update the RTC and anything else that needs a 1Hz refresh
	        if(*tick % TICKRATE == 0) { 
	            rtc_tick(rtc); 
	        }
            update_environment((uint32_t *)&environment, *tick, rtc);
            if(cons->state == WAIT) {
                // if the clock is unset flash 12:00 like a VCR
                if(rtc->flags & RTC_FLAG_UNSET) {
                    if(*tick % TICKRATE < (TICKRATE / 2)) {
                        ee_sprintf((char *)&channels[0].data, "    ");
                    }
                    else {
                        ee_sprintf((char *)&channels[0].data, "%02u%02u", 12, 00);
                    }
                }
                else {
                    // otherwise show the time
                    if(!(rtc->flags & RTC_FLAG_12HR)) {
                        ee_sprintf((char *)channels[0].data, "%02u%02u", rtc->hours, rtc->minutes);
                    }
                    else {
                        ee_sprintf((char *)channels[0].data, "%2u%02u", (rtc->hours == 0 ? 12 : rtc->hours % 12), rtc->minutes);
                    }   
                }
                // render display
                channel_render(&channels[0], (uint32_t *)&environment, (fix16_t *)&scratchpad);

                channels[1].flags = CHAN_FLAG_PAT; // pattern generator mode
                channels[1].patlen = 2;
                channels[1].data[0] = 0x66;
                channels[1].data[1] = 0x66;

                channels[1].fgcolor = 0x00060604;
                channels[1].bgcolor = 0x00000000;
                channel_render(&channels[1], (uint32_t *)&environment, (fix16_t *)&scratchpad);
            }            
        }
	}
}

//  ######  ##       #### 
// ##    ## ##        ##  
// ##       ##        ##  
// ##       ##        ##  
// ##       ##        ##  
// ##    ## ##        ##  
//  ######  ######## #### 

void dump(void) {
    char *outbuf = get_outbuf_ptr();
    volatile uint32_t *tick = (volatile uint32_t *)get_tick_ptr();
    rtc_t *rtc = (rtc_t *)get_rtc_ptr();

    uint8_t pl, channel, mi, msgsum;

    uart_puts("\r\n# configuration\r\n");
    uart_reset_checksum();
    uart_puts("{");
    uart_puts("\"system\":{");
    ee_sprintf(outbuf, "\"time\":\"%02u:%02u:%02u\",", rtc->hours, rtc->minutes, rtc->seconds);
    uart_puts(outbuf);
    ee_sprintf(outbuf, "\"time_set\":\"%c\",", rtc->flags & RTC_FLAG_UNSET ? 'N' : 'Y');
    uart_puts(outbuf);
    ee_sprintf(outbuf, "\"uptime\":\"%02u:%02u:%02u\",", *tick / 108000 % 60, *tick / 1800 % 60, *tick / 30 % 60);
    uart_puts(outbuf);
    ee_sprintf(outbuf, "\"channels\":%i,", NCHANNELS); 
    uart_puts(outbuf);
    ee_sprintf(outbuf, "\"programs\":%i,", NPROGS); 
    uart_puts(outbuf);  
    ee_sprintf(outbuf, "\"programsize\":%i,", PROG_SIZE); 
    uart_puts(outbuf);
    ee_sprintf(outbuf, "\"modsperchannel\":%i,", CHAN_MOD_SLOTS); 
    uart_puts(outbuf);
    ee_sprintf(outbuf, "\"modulators\":%i},", NMODULATORS); 
    uart_puts(outbuf);

    uart_puts("\"channel\":[");
    for(channel = 0; channel < NCHANNELS; channel++) {
        uart_puts("{");
        ee_sprintf(outbuf, "\"id\":%i,", channel);
        uart_puts(outbuf);
        uart_puts("\"modulators\":[");
        for(mi = 0; mi < CHAN_MOD_SLOTS; mi++) {
            ee_sprintf(outbuf, "\"0x%p\"", channels[channel].mods[mi]); 
            uart_puts(outbuf);
            if(mi < CHAN_MOD_SLOTS-1) uart_puts(", ");
        }
        uart_puts("],\"programs\":[");
        for(mi = 0; mi < NPROGS; mi++) {
            ee_sprintf(outbuf, "%i", channels[channel].argprog[mi]); 
            uart_puts(outbuf);
            if(mi < NPROGS-1) uart_puts(", ");
        }
        uart_puts("]}");
        if(channel < NCHANNELS-1) uart_puts(",");
    }
    uart_puts("],\"environment\":[");
    for(pl = 0; pl < ENV_SIZE; pl++) {
        ee_sprintf(outbuf, "%i", environment[pl]); 
        uart_puts(outbuf);
        if(pl < ENV_SIZE-1) uart_puts(",");
    }

    uart_puts("],\"scratch\":[");
    for(pl = 0; pl < SCRATCH_SIZE; pl++) {
        ee_sprintf(outbuf, "%i", scratchpad[pl]); 
        uart_puts(outbuf);
        if(pl < SCRATCH_SIZE-1) uart_puts(",");
    }
    uart_puts("],");
    uart_puts("\"programs\":[");
    for(pl = 0; pl < NPROGS; pl++) {
        ee_sprintf(outbuf, "\"%s\"", programs[pl]);
        uart_puts(outbuf);
        if(pl < NPROGS-1) uart_puts(",");
    }
    uart_puts("]}");
    msgsum = uart_reset_checksum();
    uart_puts("\r\n");
    ee_sprintf(outbuf, "# checksum:%02X", msgsum);
    uart_puts(outbuf);
    outbuf[0] = '\0';
}

void dump_ram(void) {
    uint32_t *base, ctr, i;
    char *outbuf = get_outbuf_ptr();
    extern uint32_t __ram_start;
    extern uint32_t __ram_size;
    extern uint32_t __stack_size;
    extern uint32_t __stack_start;
    base = &__ram_start;
    ee_sprintf(outbuf, "\r\norg: 0x%08X, size: %04u, stack: 0x%08X, stacksize: %04u\r\n", base, &__ram_size, &__stack_start, &__stack_size);
    uart_puts(outbuf);    
    
    uart_puts("\r\n");
    for(ctr = 0; ctr < 1024;) {
        ee_sprintf(outbuf, "%04X:", ctr * 4);
        uart_puts(outbuf);  
        for(i = 0; i < 4; i++) {
            ee_sprintf(outbuf, "%08x ", *(base + ctr + i));
            uart_puts(outbuf);  
        }
        ctr += i;
        ee_sprintf(outbuf, "\r\n");
        uart_puts(outbuf);                
    }
}

uint8_t read_int(char *str, int32_t *val, char terminator) {
    uint8_t c = 0;
    char buf[16+1];
    uint8_t bufp = 0;

    if((str[c] >= '0' && str[c] <= '9') || str[c] == ' ' || str[c] == '-' || str[c] == '.') { 
        while(str[c] != terminator && str[c] != 0 && str[c] != '\n' && str[c] != ';' && bufp < 16) {
            if(str[c] != ' ') {
                buf[bufp++] = str[c];
            }
            c++;
        }
        buf[bufp++] = '\0';
    }

    if(bufp > 0) {
        *val = atoi(buf);
    }
    else {
        *val = 0;
    }

    return bufp;
}

void read_cmd(char *cmdstr) {
    state_t state = IDLE;

    rtc_t *rtc = (rtc_t *)get_rtc_ptr();
    char *outbuf = (char *)get_outbuf_ptr();

    uint8_t len = strlen(cmdstr);
    uint8_t c;

    for(c = 0; c < len; c++) {
        if(state == IDLE && len > 2) {
            if((cmdstr[c++] == 'A' && cmdstr[c++] == 'T')) {
                state = STARTCMD;
            }
        }
        switch(cmdstr[c++]) {
            case '\t':
            case ' ':
                continue;
            
            case ';':
                state = STARTCMD;
                break;

            case '\n':
            case '\0':
                state = ENDCMD;
                break;
            case 'L': ;
            {
                int32_t index;
                uint8_t sr, pc = 0, read = 0;
                read = read_int(cmdstr + c, &index, ',');
                c += read;                  
                sr = c;
                for(c; (c < sr + PROG_SIZE) && cmdstr[c] != 0 && cmdstr[c] != '\n' && cmdstr[c] != '\r' && cmdstr[c] != ';'; c++) {
                    programs[index][pc++] = cmdstr[c];
                }
                programs[index][pc] = '\0';
                state = STARTCMD;
            }
            break;

            case 'C':
                dump();
                state = STARTCMD;
                break;
            case 'D':
                dump_ram();
                state = STARTCMD;
                break;
            case 'M': ; // modulator mapping
            {
                uint8_t read, mi;
                int32_t channel, mod_index;
                read = read_int(cmdstr + c, &channel, ',');
                if(read == 0) {
                    ee_sprintf(outbuf, "FAIL BAD CHAN");   
                    state = FAIL;
                    break; 
                }
                c += read;
                for(mi = 0; mi < CHAN_MOD_SLOTS; mi++) {
                    read = read_int(cmdstr + c, &mod_index, ',');
                    c += read;
                    if(read == 0) { c++; continue; }
                    channels[channel].mods[mi] = modulators[mod_index];
                }
                state = STARTCMD;
            }
            break;

            case 'P': ; // program mapping
            {
                uint8_t read, ci;
                int32_t channel, prog_index = 0;
                read = read_int(cmdstr + c, &channel, ',');
                if(read == 0) { 
                    ee_sprintf(outbuf, "FAIL BAD CHAN");  
                    state = FAIL;
                    break; 
                }
                c += read;
                for(ci = 0; ci < NPROGS; ci++) {
                    read = read_int(cmdstr + c, &prog_index, ',');
                    c += read;
                    if(read == 0) { c++; continue; }
                    
                    channels[channel].argprog[ci] = prog_index - 1;
                }
                state = STARTCMD;
            }
            break;

            case 'S': ; // set system property
            {
                if(cmdstr[c] != 0) {
                    switch(cmdstr[c++]) {
                        case 'T': ; // time
                        {
                            int32_t h, m, s, err;
                            uint8_t read;
                            read = c;
                            c += read_int(cmdstr + c, &h, ',');
                            c += read_int(cmdstr + c, &m, ',');
                            c += read_int(cmdstr + c, &s, 0);
                            if(c - read < 3) {
                                ee_sprintf(outbuf, "TIME %02u:%02u:%02u", rtc->hours, rtc->minutes, rtc->seconds);
                                if(rtc->flags & RTC_FLAG_UNSET) ee_sprintf(outbuf, "%s UNSET", outbuf); // m^..^m o hai str cat
                                break;
                            }
                            err = rtc_set(rtc, (uint8_t)h, (uint8_t)m, (uint8_t)s);
                            if(err) {
                                ee_sprintf(outbuf, "BADARG %02u:%02u:%02u", h, m, s); 
                                state = FAIL;
                                break;   
                            }
                            else {
                                ee_sprintf(outbuf, "SET %02u:%02u:%02u", rtc->hours, rtc->minutes, rtc->seconds);
                                state = STARTCMD;
                                break;         
                            }
                        }
                        break;
                        case 'F': ; // time format
                        {
                            int32_t format;
                            uint8_t read;
                            read = read_int(cmdstr + c, &format, 0);
                            c += read;
                            if(format == 12) {
                                rtc->flags |= RTC_FLAG_12HR;
                            } 
                            else {
                                rtc->flags &= ~(RTC_FLAG_12HR);
                            }
                            state = STARTCMD;
                            break;
                        }                           
                        default:
                            state = FAIL;
                    }
                }
            }    
            break;

            case 'W': ; // write register
            {
                uint32_t *reg;
                int32_t index, value;
                char regname = cmdstr[c];
                if(cmdstr[c] != 0) {
                    switch(cmdstr[c++]) {
                        case 'E': // environment register file
                            reg = &environment[0];
                        case 'S': // scratch register file
                            if(reg == NULL) reg = &scratchpad[0];
                            c += read_int(cmdstr + c, &index, ',');
                            c += read_int(cmdstr + c, &value, 0);
                            reg[index] = value;
                            ee_sprintf(outbuf, "%c[%u]=%u", regname, index, reg[index]);
                            state = STARTCMD;
                            break;
                        default:
                            ee_sprintf(outbuf, "NXREG:%c", regname);
                            state = FAIL;
                    }
                }
             }
            break;

            case 'R': ; // read register
            {
                uint32_t *reg = NULL;
                char regname = cmdstr[c];
                int32_t index, value;
                if(cmdstr[c] != 0) {
                    switch(cmdstr[c++]) {
                        case 'E': // environment register file
                            reg = environment; 
                        case 'S': // scratch register file
                            if(reg == NULL) { reg = &scratchpad; } 
                            c += read_int(cmdstr + c, &index, ',');
                            value = reg[index];
                            ee_sprintf(outbuf, "%c[%u]=%u", regname, index, value);
                            state = STARTCMD;
                            break;
                        default:
                            ee_sprintf(outbuf, "NXREG:%c", regname);
                            state = FAIL;
                    }
                }
            }
            break;
            default:
                continue;
        }
        c--;

        if(state == FAIL) {
            // throw error then go idle
            uart_puts("\r\n! ");
            uart_puts(outbuf);
            uart_puts("\r\n");
            ee_sprintf(outbuf, "FAIL CHAR:%u:>%s\n\r", c, cmdstr + c - 1);
            uart_puts(outbuf);
            state = IDLE;
            break;
        }

        if(state == STARTCMD) {
            uart_puts("\r\n# ");
            uart_puts(outbuf);
            uart_puts("\r\n");
        }
    }
    uart_puts("OK");
}